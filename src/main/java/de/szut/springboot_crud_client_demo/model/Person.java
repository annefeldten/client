package de.szut.springboot_crud_client_demo.model;

public class Person {
	
	private int id;
	private String firstname;
	private String surname;

	public Person() {
	}

	public Person(int id, String firstname, String surname) {
		this.id = id;
		this.firstname = firstname;
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "Person: ";
		text += "\n\tid = " + id;
		text += "\n\tVorname = " + firstname;
		text += "\n\tNachname = " + surname;
		return text;
	}
	
}
