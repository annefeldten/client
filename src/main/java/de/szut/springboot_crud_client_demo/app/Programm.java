package de.szut.springboot_crud_client_demo.app;

import de.szut.springboot_crud_client_demo.dao.PersonDao;
import de.szut.springboot_crud_client_demo.model.Person;

import java.util.ArrayList;
import java.util.List;

public class Programm {

	public static void main(String[] args) {
		PersonDao dao = new PersonDao();
		List<Person> personenListe = new ArrayList<Person>();
		personenListe = dao.readAll();
		printPersonenListe(personenListe);
		Person newPerson = new Person(4, "Helge", "Possehl");
		dao.create(newPerson);
		printPerson(newPerson);
		personenListe = dao.readAll();
		printPersonenListe(personenListe);
		newPerson.setId(0);
		dao.deleteByParam(newPerson.getId());
		personenListe = dao.readAll();
		printPersonenListe(personenListe);
		newPerson = dao.readByUrl(4);
		printPerson(newPerson);
		newPerson.setSurname("Mustermann");
		printPerson(newPerson);
		dao.update(newPerson);
		personenListe = dao.readAll();
		printPersonenListe(personenListe);
		dao.deleteByParam(newPerson.getId());
		personenListe = dao.readAll();
		printPersonenListe(personenListe);
	}
	
	private static void printPerson(Person person) {
		System.out.println(person);
	}
	
	private static void printPersonenListe(List<Person> personenListe) {
		for (Person person : personenListe) {
			System.out.println(person);
		}
	}

}
